/**
 * index.js
 *
 * Saravjeet Singh
 * <saravjeetamansingh@gmail.com>
 *
 */

/* helper */

function randInt(n) { 
    // bitwise OR removes fractional part of the number
    return (Math.random() * n) | 0; 
}
/**
 * actor handles a particular instance of a sprite
 * and handles drawing it on every frame.
 */


function FireworkActor(screenWidth, screenHeight) {
    this.x = Math.random() * screenWidth;
    this.y = Math.random() * screenHeight;

    var xVector = [0, screenWidth]; 
    var yVector = [0, screenHeight];
    
    var base = randInt(2);
    // if base > 0, use x as the base, else y
    if ( base ) {
        var i = randInt(xVector.length);
        this.startx = xVector[i];
        this.starty = randInt(screenHeight);
    } else {
        var i = randInt(xVector.length);
        this.startx = randInt(screenWidth);
        this.starty = yVector[i];
    }

    this.currentx = this.startx;
    this.currenty = this.starty;

    // speed is the number of frames it takes for the shooting animation
    // to complete
    var moveSpeed = 400; 
    var explosionSpeed = 1800;

    // compute vectors
    this.speedx = (this.x - this.startx) / moveSpeed;
    this.speedy = (this.y - this.starty) / moveSpeed;
    // the shooting animation has completed
    this.readyToExplode = false;
    // the explosive animation has completed
    this.thatsAllFolks = false;

    // explosion parameters
    this.currentRadius = 5;
    this.finalRadius = screenWidth / 3;
    this.explosionRate = (this.finalRadius - this.currentRadius) / explosionSpeed;
    var r,g,b;
    // bitwise OR with 0 will convert
    // floating point values to ints
    r = (Math.random() * 255) | 0;
    g = (Math.random() * 255) | 0;
    b = (Math.random() * 255) | 0;
    // ouch! me eyes
    this.explosionColor = "rgb("+r+","+g+","+b+")";

}


FireworkActor.prototype.draw = function(ctx) {
    if ( this.readyToExplode) {
        ctx.beginPath();
        ctx.fillStyle = this.explosionColor;
        ctx.arc(this.x, this.y, this.currentRadius, 0, 2*Math.PI);
        ctx.fill();
        ctx.closePath();

        this.currentRadius = this.currentRadius + this.explosionRate;
        if ( this.currentRadius >= this.finalRadius ) {
            this.thatsAllFolks = true;
        }
    } else {
        // white
        ctx.strokeStyle = "#aeaeae";
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(this.startx, this.starty);
        ctx.lineTo(this.currentx, this.currenty);
        ctx.stroke();
        ctx.closePath();

        // update stuff
        this.currentx = this.currentx + this.speedx;
        this.currenty = this.currenty + this.speedy;

        if ( this.currenty <= this.y ) {
            this.readyToExplode = true;
        }
    }
}

/**
 * the screen object takes care of spawning actors and killing them
 * mercilessly once they've served their purpose. If I were you, I 
 * would keep my distance
 */

function Screen(maxActors) {
    var el = document.querySelector('canvas');
    this.ctx = el.getContext('2d');
    this.height = el.height;
    this.width = el.width;
    this.actors = [];
    
    // spawn delay
    this.spawnDelay = 200;
    this.lastSpawn = Date.now();
    if (maxActors) {
        this.maxActors = maxActors;
    } else {
        this.maxActors = 25;
    }
}

Screen.prototype.update = function() {
    var self = this;
    if ( this.actors.length < this.maxActors && (Date.now() - this.lastSpawn) > this.spawnDelay ) {
        this.lastSpawn = Date.now();
        this.actors.push(new FireworkActor(this.width, this.height));
    }

    this.ctx.fillStyle = "#ffffff";
    this.ctx.fillRect(0, 0, this.width, this.height);
    this.actors = this.actors.filter(function(actor) {
        actor.draw(self.ctx);
        return !actor.thatsAllFolks;
    });

    // hard code. no hate pls.

    this.ctx.font = "72px monospace";
    this.ctx.textAlign = "center";
    this.ctx.fillStyle = "#ffffff";
    this.ctx.fillText("Happy", this.width/2, this.height/4);
    this.ctx.fillText("Birthday", this.width/2, this.height/2);
    this.ctx.fillText("KT & Meg",this.width/2, this.height/1.3);
}

document.querySelector('button.start').onclick = function(e) {
    this.style.display = "none";
    var audio = document.querySelector('audio');
    audio.addEventListener('ended', function(e) {
        var bloopers = document.querySelectorAll('.blooper');
        for (var i = 0; i < bloopers.length; ++i) {
            bloopers[i].style.display = "block";
        }
        var tmp = document.querySelectorAll('.tmp');
        for (var i = 0; i < tmp.length; ++i) {
            tmp[i].style.display = "none";
        }
    });
    document.querySelector('audio').play();
    var screen = new Screen(50);
    setInterval(function() { screen.update(); }, 1.0/10.0);
}
